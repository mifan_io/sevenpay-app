/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, Image, View, TouchableHighlight} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Button from 'react-native-button';
import FontAwesome, { Icons } from 'react-native-fontawesome';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
type Props = {};
export default class App extends Component<Props> {
  constructor(props, context) {
    super(props, context);
  }
  _handlePress() {
    console.log('Pressed!');
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.account}>
            <View style={styles.headerMenuLeft}>
              <TouchableHighlight>
              <Text style={styles.accountHeader}><FontAwesome>{Icons.chevronCircleDown}</FontAwesome></Text>
              </TouchableHighlight>
            </View>
            <View style={styles.headerMenuRight}>
              <Text style={styles.accountHeaderRight}><FontAwesome>{Icons.addressBook}</FontAwesome></Text>
            </View>
          </View>
        </View>
        <View style={styles.account}>
            <View style={styles.accountDescription}>
              <Text style={styles.accountHeader}>BALANCE :</Text>
              <Text style={styles.accountText}>Rp. 0,-</Text>
            </View>
            <View style={{
                  backgroundColor: '#FF8600', 
                  marginRight: wp('10%'), 
                  width: '30%',
                  height: hp('6.5%'), 
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
              <Button
                style={{
                  fontSize: 15, 
                  color: '#331832',
                }}
                onPress={() => this._handlePress()}>
                ISI SALDO
              </Button>
            </View>
        </View>
        <View style={styles.content}>
          <Text style={{marginTop: hp('4%'), marginLeft: wp('10%'), color: '#331832'}}>Layanan Pembayaran :</Text>
          <View style={{flex: 1, flexDirection: 'row', marginTop: hp('5%')}}>
            <View style={styles.contentIcon}>
              <Text style={styles.contentText}>Pulsa</Text>
            </View>
            <View style={styles.contentIcon}>
              <Text style={styles.contentText}>Listrik</Text>
            </View>
            <View style={styles.contentIcon}>
              <Text style={styles.contentText}>PDAM</Text>
            </View>
            <View style={styles.contentIcon}>
              <Text style={styles.contentText}>DLL</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: hp('40%'),
    width: wp('100%'),
    backgroundColor: '#331832',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  headerMenuLeft: {
    height: wp('50%'),
    flex: 1,
    marginLeft: wp('10%'),
    marginTop: hp('3%'),
  },
  headerMenuRight: {
    height: wp('50%'),
    flex: 1,
    marginRight: wp('10%'),
    marginTop: hp('3%'),
  },
  account: {
    height: hp('15%'),
    width: wp('100%'),
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#331832',
  },
  accountDescription: {
    height: wp('50%'),
    flex: 1,
    marginLeft: wp('10%')
  },
  accountHeader: {
    fontSize: 13,
    color: '#FF8600',
  },
  accountHeaderRight: {
    fontSize: 13,
    color: '#FF8600',
    textAlign: 'right',
  },
  accountText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  content: {
    height: hp('45%'),
    width: wp('100%'),
    backgroundColor: '#FFF',
  },
  contentIcon: {
    height: wp('25%'),
    width: wp('25%'),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentText: {
    fontSize: 12,
    marginTop: hp('1%'),
    fontWeight: 'bold',
    color: '#331832',
  },
  contentImage: {
  },
});
